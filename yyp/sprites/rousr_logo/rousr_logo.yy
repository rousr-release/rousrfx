{
    "id": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rousr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 51,
    "bbox_right": 99,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c03ffdb-14f3-4a0b-bfdd-25f9db6647cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
            "compositeImage": {
                "id": "4d6466d9-bb9e-4ae7-9541-57f2a1b3ed01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c03ffdb-14f3-4a0b-bfdd-25f9db6647cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac09d26d-7423-4be0-889e-ead57f99b6bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c03ffdb-14f3-4a0b-bfdd-25f9db6647cc",
                    "LayerId": "9aa9079b-3d29-4f07-85e7-1ccacb1068cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "9aa9079b-3d29-4f07-85e7-1ccacb1068cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 36,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}