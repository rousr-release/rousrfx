{
    "id": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "the_intrinsic_value_of_breath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5faa1aa-46fc-42c4-bfda-b846587a7b2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
            "compositeImage": {
                "id": "563d530c-4366-48e9-8789-9ab2910aea45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5faa1aa-46fc-42c4-bfda-b846587a7b2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1efec47e-1096-4c50-9c4b-603555545850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5faa1aa-46fc-42c4-bfda-b846587a7b2e",
                    "LayerId": "c5b7d017-5b00-48af-a4d0-b9c9fb14ba04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c5b7d017-5b00-48af-a4d0-b9c9fb14ba04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}