{
    "id": "1c699b67-2065-4e1c-b68d-177b0df54bf4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Bloom",
    "eventList": [
        {
            "id": "a68eb2d7-a687-40b7-adad-875fea1c9720",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "7c74c5af-9453-44d0-a709-aeedcabd5bdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "1bde1411-3ac5-4eac-9694-a18b4c9da2c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "deac0616-9f3c-461f-acb6-9d51174c52b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 113,
            "eventtype": 9,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "b658862e-7d84-4bed-82c6-86e4052a83d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
    "visible": true
}