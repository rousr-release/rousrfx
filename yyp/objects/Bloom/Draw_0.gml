///@desc 
var label = "";
switch (Bloom_State) {
  case EBloom_Presets.Default:     label = "Default"; break;
  case EBloom_Presets.Soft:        label = "Soft"; break;
  case EBloom_Presets.Desaturated: label = "Desaturated"; break;
  case EBloom_Presets.Saturated:   label = "Saturated"; break;
  case EBloom_Presets.Blurry:      label = "Blurry"; break;
  case EBloom_Presets.Subtle:      label = "Subtle"; break;

  default: label = "None";
}

draw_set_font(fnt_game);
label = "Bloom: " + label;
var w = string_width(label);
draw_text((Screen_Width - w) * 0.5, Screen_Height - 15, label);

//draw_self();