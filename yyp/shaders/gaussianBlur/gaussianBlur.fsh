//guassianBlur.fsh
//
// Pixel shader applies a one dimensional gaussian blur filter.
// This is used twice by the bloom postprocess, first to
// blur horizontally, and then again to blur vertically.
//
varying vec2 v_vTexcoord;
const int SAMPLE_COUNT = 15;

uniform float SampleOffsetsX[SAMPLE_COUNT];
uniform float SampleOffsetsY[SAMPLE_COUNT];
uniform float SampleWeights[SAMPLE_COUNT];

void main()
{
  gl_FragColor = vec4(0,0,0,0);
  for (int i = 0; i < SAMPLE_COUNT; ++i)
    gl_FragColor += texture2D( gm_BaseTexture, v_vTexcoord + vec2(SampleOffsetsX[i], SampleOffsetsY[i])) * SampleWeights[i];
}
