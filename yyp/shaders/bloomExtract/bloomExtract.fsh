// bloomExtract.fsh
//
// Pixel shader extracts the brighter areas of an image.
// This is the first step in applying a bloom postprocess.
//
varying vec2 v_vTexcoord;
uniform float BloomThreshold;

const vec4 v4zero = vec4(0.0);
const vec4 v4one  = vec4(1.0);

vec4 saturate(vec4 c) {
  return clamp(c, v4zero, v4one);
}

void main()
{
  vec4 c = texture2D( gm_BaseTexture, v_vTexcoord );
  vec4 threshold = vec4(BloomThreshold);
  gl_FragColor = saturate((c - threshold) / (v4one - threshold));
}