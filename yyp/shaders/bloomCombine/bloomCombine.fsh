//
// Pixel shader combines the bloom image with the original
// scene, using tweakable intensity levels and saturation.
// This is the final step in applying a bloom postprocess.
//
varying vec2 v_vTexcoord;

uniform float BloomIntensity;
uniform float BaseIntensity;

uniform float BloomSaturation;
uniform float BaseSaturation;

uniform sampler2D BloomSampler;

const vec4 v4one  = vec4(1.0);
const vec4 v4zero = vec4(0.0);

vec4 saturate(vec4 c) { return clamp(c, v4zero, v4one); }

// Helper for modifying the saturation of a color.
vec4 AdjustSaturation(vec4 c, float saturation)
{
  // The constants 0.3, 0.59, and 0.11 are chosen because the
  // human eye is more sensitive to green light, and less to blue.
  float grey = dot(c.xyz, vec3(0.3, 0.59, 0.11));

  return mix(vec4(grey), c, saturation);
}

void main()
{
  vec4 bloom = texture2D(BloomSampler, v_vTexcoord);
  vec4 base  = texture2D(gm_BaseTexture, v_vTexcoord);
    
  // Adjust color saturation and intensity.
  bloom = saturate(AdjustSaturation(bloom, BloomSaturation) * BloomIntensity);
  base  = saturate(AdjustSaturation(base, BaseSaturation) * BaseIntensity);
    
  // Darken down the base image in areas where there is a lot of bloom,
  // to prevent things looking excessively burned-out.
  base *= (v4one - bloom);
    
  // Combine the two images.
  gl_FragColor = saturate(base) + bloom;
}
