///@func gaussian_set_params(_dx, _dy, _blur_amount)
///@desc Computes sample weightings and texture coordinate offsets for one pass of a separable gaussian blur filter.
///@param {Real} _dx
///@param {Real} _dy
///@param {Real} _blur_amount
///@returns {Array} [ offsets, weights ]
var _dx = argument0,
    _dy = argument1,
  theta = argument2;

// todo: memoize results
var sampleCount = 15;

// todo: hash this better
var _param_id = string(_dx * 6) + " " + string(_dy * 6) + " " + string(theta * 4);// + " " + eyeglass_get_pos_string() ;
var _params = global.Blur_params[? _param_id];

if (_params == undefined) {

  // Create temporary arrays for computing our filter settings.
  var sampleWeights  = array_create(sampleCount);
  var sampleOffsetsX = array_create(sampleCount);
  var sampleOffsetsY = array_create(sampleCount);

  // The first sample always has a zero offset
  sampleWeights[@ 0]  = (1.0 / sqrt(2 * pi * theta));
  sampleOffsetsX[@ 0] = 0;
  sampleOffsetsY[@ 0] = 0;

  // Maintain a sum of all the weighting values.
  var totalWeights = sampleWeights[0];

  // Add pairs of additional sample taps, positioned
  // along a line in both directions from the center.
  for (var i = 0; i < (sampleCount - 1) / 2; ++i)
  {
    var index1 = (i * 2) + 1;
    var index2 = (i * 2) + 2;

    var n = i + 1;  
    var weight = (1.0 / sqrt(2 * pi * theta)) * exp(-(n * n) / (2 * theta * theta));

    // Store weights for the positive and negative taps.
    sampleWeights[@ index1] = weight;
    sampleWeights[@ index2] = weight;

    totalWeights += weight * 2;

    // To get the maximum amount of blurring from a limited number of
    // pixel shader samples, we take advantage of the bilinear filtering
    // hardware inside the texture fetch unit. If we position our texture
    // coordinates exactly halfway between two texels, the filtering unit
    // will average them for us, giving two samples for the price of one.
    // This allows us to step in units of two texels per sample, rather
    // than just one at a time. The 1.5 offset kicks things off by
    // positioning us nicely in between two texels.
    var sampleOffset = i * 2 + 1.5;

    // Store texture coordinate offsets for the positive and negative taps.
    sampleOffsetsX[@ index1] = _dx * sampleOffset;
    sampleOffsetsY[@ index1] = _dy * sampleOffset;

    sampleOffsetsX[@ index2] = -_dx * sampleOffset;
    sampleOffsetsY[@ index2] = -_dy * sampleOffset;
  
  }

  // Normalize the list of sample weightings, so they will always sum to one.
  for (var i = 0; i < sampleCount; i++)
  {
    sampleWeights[@ i] /= totalWeights;
  }

  _params = [ sampleOffsetsX, sampleOffsetsY, sampleWeights ];
  global.Blur_params[? _param_id] = _params;
}

return _params;